package com.lsoehadak.moviecatalogue.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lsoehadak.moviecatalogue.R;
import com.lsoehadak.moviecatalogue.database.DatabaseHelper;
import com.lsoehadak.moviecatalogue.database.MovieHelper;
import com.lsoehadak.moviecatalogue.model.Movie;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
    private List<Movie> movies = new ArrayList<>();
    private Context context;

    StackRemoteViewsFactory(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate() {
//        MovieHelper.getInstance(context).open();
        movies = MovieHelper.getInstance(context).getFavorites(Movie.TYPE_MOVIE);
    }

    @Override
    public void onDataSetChanged() {
//        Toast.makeText(context, "Widget updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.item_widget);
        rv.setTextViewText(R.id.tv_movie_title, movies.get(i).getTitle());
        Bitmap bitmap;
        try {
            bitmap = Glide.with(context).asBitmap().load("https://image.tmdb.org/t/p/w92" + movies.get(i).getPosterUrl()).submit().get();
            rv.setImageViewBitmap(R.id.iv_movie_poster, bitmap);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        Log.e("load ", "widget ke " + i);

//        Bundle extras = new Bundle();
//        extras.putInt(FavoriteMovieWidget.EXTRA_ITEM, i);
//        Intent fillInIntent = new Intent();
//        fillInIntent.putExtras(extras);
//
//        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
