package com.lsoehadak.moviecatalogue;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lsoehadak.moviecatalogue.adapter.SearchResultPagerAdapter;
import com.lsoehadak.moviecatalogue.model.Movie;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {
    private final String MOVIE_LIST = "movie_list";
    private final String TV_SHOW_LIST = "tv_show_list";

    private ConstraintLayout contentContainer;
    private TabLayout tl;
    private ViewPager vpResult;
    private View noContentContainer;
    private ImageView ivBack, ivClearSearch, ivErrorIcon;
    private EditText etMovieTitle;
    private TextView tvErrorMessage;
    private Button btnRefresh;
    private ProgressBar pb;

    private ArrayList<Movie> movies = new ArrayList<>();
    private ArrayList<Movie> tvShows = new ArrayList<>();
    private SearchResultPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ivBack = findViewById(R.id.iv_back);
        etMovieTitle = findViewById(R.id.et_movie_title);
        ivClearSearch = findViewById(R.id.iv_clear_search);

        contentContainer = findViewById(R.id.content_container);
        tl = findViewById(R.id.tl);
        vpResult = findViewById(R.id.vp_result);

        noContentContainer = findViewById(R.id.no_content_container);
        ivErrorIcon = noContentContainer.findViewById(R.id.iv_error_icon);
        tvErrorMessage = noContentContainer.findViewById(R.id.tv_error_message);
        btnRefresh = noContentContainer.findViewById(R.id.btn_refresh);
        pb = findViewById(R.id.pb);

        ivBack.setOnClickListener(this);
        ivClearSearch.setOnClickListener(this);
        btnRefresh.setOnClickListener(this);

        etMovieTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    ivClearSearch.setVisibility(View.VISIBLE);
                } else {
                    ivClearSearch.setVisibility(View.GONE);
                }
            }
        });

        etMovieTitle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    if (etMovieTitle.getText().length() > 0) {
                        startSearch();
                    } else {
                        Toast.makeText(SearchActivity.this, getResources().getString(R.string.search_keyword_empty),
                                Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });

        etMovieTitle.requestFocus();
        setPagerAdapter();

        if (savedInstanceState != null) {
            movies = savedInstanceState.getParcelableArrayList(MOVIE_LIST);
            tvShows = savedInstanceState.getParcelableArrayList(TV_SHOW_LIST);

            adapter.setResultData(movies, tvShows, 2);

            contentContainer.setVisibility(View.VISIBLE);
            noContentContainer.setVisibility(View.GONE);
            pb.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(MOVIE_LIST, movies);
        outState.putParcelableArrayList(TV_SHOW_LIST, tvShows);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_back) {
            finish();
        } else if (view.getId() == R.id.iv_clear_search) {
            etMovieTitle.setText("");
        }
    }

    private void startSearch() {
        etMovieTitle.setEnabled(false);

        movies.clear();
        tvShows.clear();

        contentContainer.setVisibility(View.GONE);
        noContentContainer.setVisibility(View.GONE);
        pb.setVisibility(View.VISIBLE);
        searchMovies();
    }

    private void setPagerAdapter() {
        adapter = new SearchResultPagerAdapter(
                getSupportFragmentManager(), 2,
                movies, tvShows);
        vpResult.setAdapter(adapter);
        vpResult.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tl));
        tl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpResult.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void searchMovies() {
        ApiInterface apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiInterface.searchMovie(
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.language),
                etMovieTitle.getText().toString());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    try {
                        JsonArray dataArr = response.body().get("results").getAsJsonArray();
                        if (dataArr.size() > 0) {
                            for (int i = 0; i < dataArr.size(); i++) {
                                JsonObject dataObj = dataArr.get(i).getAsJsonObject();
                                Movie movie = new Movie(
                                        dataObj.get("id").getAsInt(),
                                        dataObj.get("title").getAsString(),
                                        dataObj.get("overview").getAsString(),
                                        dataObj.get("poster_path").isJsonNull() ? "" : dataObj.get("poster_path").getAsString()
                                );
                                movies.add(movie);
                            }
                        }
                        searchTvShows();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    etMovieTitle.setEnabled(true);

                    pb.setVisibility(View.GONE);
                    contentContainer.setVisibility(View.GONE);
                    noContentContainer.setVisibility(View.VISIBLE);
                    ivErrorIcon.setImageResource(R.drawable.ic_error_outline_black_24dp);
                    tvErrorMessage.setText(R.string.response_unsuccessful);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                etMovieTitle.setEnabled(true);

                pb.setVisibility(View.GONE);
                contentContainer.setVisibility(View.GONE);
                noContentContainer.setVisibility(View.VISIBLE);
                ivErrorIcon.setImageResource(R.drawable.ic_signal_cellular_connected_no_internet_1_bar_black_24dp);
                tvErrorMessage.setText(R.string.response_connection_error);
            }
        });
    }

    private void searchTvShows() {
        ApiInterface apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiInterface.searchTvShow(
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.language),
                etMovieTitle.getText().toString());

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    etMovieTitle.setEnabled(true);
                    try {
                        JsonArray dataArr = response.body().get("results").getAsJsonArray();
                        if (dataArr.size() > 0) {
                            for (int i = 0; i < dataArr.size(); i++) {
                                JsonObject dataObj = dataArr.get(i).getAsJsonObject();
                                Movie movie = new Movie(
                                        dataObj.get("id").getAsInt(),
                                        dataObj.get("name").getAsString(),
                                        dataObj.get("overview").getAsString(),
                                        dataObj.get("poster_path").isJsonNull() ? "" : dataObj.get("poster_path").getAsString()
                                );
                                tvShows.add(movie);
                            }
                        }

                        adapter.setResultData(movies, tvShows, 2);
                        contentContainer.setVisibility(View.VISIBLE);
                        noContentContainer.setVisibility(View.GONE);
                        pb.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    etMovieTitle.setEnabled(true);

                    pb.setVisibility(View.GONE);
                    contentContainer.setVisibility(View.GONE);
                    noContentContainer.setVisibility(View.VISIBLE);
                    ivErrorIcon.setImageResource(R.drawable.ic_error_outline_black_24dp);
                    tvErrorMessage.setText(R.string.response_unsuccessful);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                etMovieTitle.setEnabled(true);

                pb.setVisibility(View.GONE);
                contentContainer.setVisibility(View.GONE);
                noContentContainer.setVisibility(View.VISIBLE);
                ivErrorIcon.setImageResource(R.drawable.ic_signal_cellular_connected_no_internet_1_bar_black_24dp);
                tvErrorMessage.setText(R.string.response_connection_error);
            }
        });
    }
}
