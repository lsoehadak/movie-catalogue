package com.lsoehadak.moviecatalogue.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.lsoehadak.moviecatalogue.fragment.MovieListFragment;
import com.lsoehadak.moviecatalogue.fragment.TvShowListFragment;

public class MainPagerAdapter extends FragmentPagerAdapter {
    private int numOfPages;

    public MainPagerAdapter(FragmentManager fm, int numOfPages) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.numOfPages = numOfPages;
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            return new MovieListFragment();
        }
        return new TvShowListFragment();
    }

    @Override
    public int getCount() {
        return numOfPages;
    }
}
