package com.lsoehadak.moviecatalogue.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.lsoehadak.moviecatalogue.fragment.FavoriteMovieListFragment;
import com.lsoehadak.moviecatalogue.fragment.FavoriteTvShowListFragment;

public class FavoritePagerAdapter extends FragmentPagerAdapter {
    private int numOfPages;

    public FavoritePagerAdapter(FragmentManager fm, int numOfPages) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.numOfPages = numOfPages;
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            return new FavoriteMovieListFragment();
        }
        return new FavoriteTvShowListFragment();
    }

    @Override
    public int getCount() {
        return numOfPages;
    }
}
