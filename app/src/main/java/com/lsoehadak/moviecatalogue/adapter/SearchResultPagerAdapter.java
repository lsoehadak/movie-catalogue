package com.lsoehadak.moviecatalogue.adapter;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.lsoehadak.moviecatalogue.DetailMovieActivity;
import com.lsoehadak.moviecatalogue.fragment.SearchResultListFragment;
import com.lsoehadak.moviecatalogue.model.Movie;

import java.util.ArrayList;

public class SearchResultPagerAdapter extends FragmentPagerAdapter {
    private int numOfPages;
    private ArrayList<Movie> movies, tvShows;

    public SearchResultPagerAdapter(@NonNull FragmentManager fm, int numOfPages,
                                    ArrayList<Movie> movies, ArrayList<Movie> tvShows) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.numOfPages = numOfPages;
        this.movies = movies;
        this.tvShows = tvShows;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(SearchResultListFragment.EXTRA_MOVIES, movies);
            bundle.putInt(DetailMovieActivity.EXTRA_TYPE, Movie.TYPE_MOVIE);
            SearchResultListFragment movieResultFragment = new SearchResultListFragment();
            movieResultFragment.setArguments(bundle);
            return movieResultFragment;
        } else {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(SearchResultListFragment.EXTRA_MOVIES, tvShows);
            bundle.putInt(DetailMovieActivity.EXTRA_TYPE, Movie.TYPE_TV);
            SearchResultListFragment movieResultFragment = new SearchResultListFragment();
            movieResultFragment.setArguments(bundle);
            return movieResultFragment;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return numOfPages;
    }

    public void setResultData(ArrayList<Movie> movies, ArrayList<Movie> tvShows, int numOfPages) {
        this.numOfPages = numOfPages;
        this.movies = movies;
        this.tvShows = tvShows;
        notifyDataSetChanged();
    }
}
