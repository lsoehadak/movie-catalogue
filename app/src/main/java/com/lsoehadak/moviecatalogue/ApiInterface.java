package com.lsoehadak.moviecatalogue;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    // get list movie
    @GET("discover/movie")
    Call<JsonObject> getMovieList(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    // get list tvShow
    @GET("discover/tv")
    Call<JsonObject> getTvShowList(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    // get detail movie
    @GET("movie/{movie_id}")
    Call<JsonObject> getMovieDetail(
            @Path("movie_id") int movieId,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    // get detail TV show
    @GET("tv/{tv_id}")
    Call<JsonObject> getTvDetail(
            @Path("tv_id") int tvId,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    // search movie
    @GET("search/movie")
    Call<JsonObject> searchMovie(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("query") String keyword
    );

    // search tvShow
    @GET("search/tv")
    Call<JsonObject> searchTvShow(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("query") String keyword
    );

    @GET("discover/movie")
    Call<JsonObject> getNewReleasedMovieList(
            @Query("api_key") String apiKey,
            @Query("primary_release_date.gte") String releaseDateGte,
            @Query("primary_release_date.lte") String releaseDateLte
    );
}
