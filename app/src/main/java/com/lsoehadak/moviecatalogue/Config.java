package com.lsoehadak.moviecatalogue;

import android.content.Context;
import android.content.SharedPreferences;

public class Config {
    private static final String PREF_NAME = "movie_catalogue_config";
    public static final String FIRST_TIME_LAUNCH = "first_time_launch";
    public static final String DAILY_REMINDER = "daily_reminder";
    public static final String RELEASE_TODAY_REMINDER = "release_today_reminder";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private static Config instance = null;

    private Config(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    static Config getInstance(Context context) {
        if(instance == null) {
            instance = new Config(context);
        }
        return instance;
    }

    void initConfig() {
        editor = sharedPreferences.edit();
        editor.putBoolean(FIRST_TIME_LAUNCH, false);
        editor.putBoolean(DAILY_REMINDER, true);
        editor.putBoolean(RELEASE_TODAY_REMINDER, true);
        editor.apply();
    }

    public void setBooleanData(String keyword, Boolean value) {
        editor = sharedPreferences.edit();
        editor.putBoolean(keyword, value);
        editor.apply();
    }

    Boolean getBooleanData(String keyword) {
        return sharedPreferences.getBoolean(keyword, true);
    }
}
