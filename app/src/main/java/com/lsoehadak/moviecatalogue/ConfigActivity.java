package com.lsoehadak.moviecatalogue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.lsoehadak.moviecatalogue.alarmmanager.AlarmReceiver;

public class ConfigActivity extends AppCompatActivity {
    private AlarmReceiver alarmReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        Switch switchDailyReminder, switchReleaseReminder;

        Toolbar tb = findViewById(R.id.tb);
        switchDailyReminder = findViewById(R.id.switch_daily_reminder);
        switchReleaseReminder = findViewById(R.id.switch_release_reminder);
        ConstraintLayout menuChangeLanguage = findViewById(R.id.menu_change_language);

        setSupportActionBar(tb);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.setting);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        alarmReceiver = new AlarmReceiver();

        switchDailyReminder.setChecked(Config.getInstance(this).getBooleanData(Config.DAILY_REMINDER));
        switchReleaseReminder.setChecked(Config.getInstance(this).getBooleanData(Config.RELEASE_TODAY_REMINDER));

        switchDailyReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Config.getInstance(ConfigActivity.this).setBooleanData(Config.DAILY_REMINDER, b);
                if (!b)
                    alarmReceiver.cancelAlarm(ConfigActivity.this, AlarmReceiver.TYPE_DAILY_REMINDER);
                else
                    alarmReceiver.setDailyReminder(ConfigActivity.this, getResources().getString(R.string.app_name), getResources().getString(R.string.daily_reminder_message));
            }
        });

        switchReleaseReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Config.getInstance(ConfigActivity.this).setBooleanData(Config.RELEASE_TODAY_REMINDER, b);
                if (!b)
                    alarmReceiver.cancelAlarm(ConfigActivity.this, AlarmReceiver.TYPE_RELEASE_TODAY_REMINDER);
                else
                    alarmReceiver.setReleaseTodayReminder(ConfigActivity.this, false);
            }
        });

        menuChangeLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }
}
