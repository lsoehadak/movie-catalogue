package com.lsoehadak.moviecatalogue;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.lsoehadak.moviecatalogue.adapter.MainPagerAdapter;
import com.lsoehadak.moviecatalogue.alarmmanager.AlarmReceiver;
import com.lsoehadak.moviecatalogue.database.MovieHelper;

public class MainActivity extends AppCompatActivity {
    private ViewPager vp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Config.getInstance(this).getBooleanData(Config.FIRST_TIME_LAUNCH)) {
            initConfig();
        }

        final Toolbar tb = findViewById(R.id.tb);
        TabLayout tl = findViewById(R.id.tl);
        vp = findViewById(R.id.vp);

        setSupportActionBar(tb);

        MovieHelper.getInstance(getApplicationContext()).open();

        MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), 2);
        vp.setAdapter(adapter);
        vp.setOffscreenPageLimit(2);

        vp.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tl));

        tl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MovieHelper.getInstance(getApplicationContext()).close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.m_setting) {
            Intent intent = new Intent(this, ConfigActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.m_favorite) {
            Intent intent = new Intent(this, FavoriteActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.m_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initConfig() {
        Config.getInstance(this).initConfig();
        if (Config.getInstance(this).getBooleanData(Config.DAILY_REMINDER)) {
            AlarmReceiver alarmReceiver = new AlarmReceiver();
            alarmReceiver.setDailyReminder(this, getResources().getString(R.string.app_name), getResources().getString(R.string.daily_reminder_message));
            alarmReceiver.setReleaseTodayReminder(this, false);
        }
    }
}
