package com.lsoehadak.moviecatalogue;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lsoehadak.moviecatalogue.database.MovieHelper;
import com.lsoehadak.moviecatalogue.model.Movie;
import com.lsoehadak.moviecatalogue.widget.FavoriteMovieWidget;

import retrofit2.Call;
import retrofit2.Callback;

public class DetailMovieActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String EXTRA_TYPE = "extra_type";
    public static final String EXTRA_ID = "extra_id";

    private final String IMAGE_URL = "image_url";
    private final String TITLE = "title";
    private final String GENRES = "genres";
    private final String RELEASE_DATE = "release_date";
    private final String RUN_TIME = "run_time";
    private final String USER_SCORE = "user_score";
    private final String OVERVIEW = "overview";
    private final String PRODUCTION_COMPANIES = "production_companies";

    private NestedScrollView contentContainer;
    private View noContentContainer;
    private ProgressBar pb;
    private TextView tvTitle, tvGenres, tvReleaseDate, tvRunTime, tvUserScore, tvOverview, tvProductionCompanies, tvErrorMessage;
    private ImageView ivAddToFavorite, ivPoster, ivErrorIcon;

    private String imageUrl, title, genres, releaseDate, runTime, userScore, overview, productionCompanies;
    private boolean isFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        Toolbar tb = findViewById(R.id.tb);
        contentContainer = findViewById(R.id.content_container);
        tvTitle = findViewById(R.id.tv_no_content_title);
        tvGenres = findViewById(R.id.tv_genres);
        ivAddToFavorite = findViewById(R.id.iv_add_to_favorite);
        ivPoster = findViewById(R.id.iv_poster);
        tvReleaseDate = findViewById(R.id.tv_release_date);
        tvRunTime = findViewById(R.id.tv_run_time);
        tvUserScore = findViewById(R.id.tv_user_score);
        tvOverview = findViewById(R.id.tv_overview);
        tvProductionCompanies = findViewById(R.id.tv_production_companies);
        noContentContainer = findViewById(R.id.no_content_container);
        ivErrorIcon = noContentContainer.findViewById(R.id.iv_error_icon);
        tvErrorMessage = noContentContainer.findViewById(R.id.tv_error_message);
        Button btnRefresh = noContentContainer.findViewById(R.id.btn_refresh);
        pb = findViewById(R.id.pb);

        setSupportActionBar(tb);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.detail);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        ivAddToFavorite.setOnClickListener(this);
        btnRefresh.setOnClickListener(this);

        // set favorite icon drawable
        if (MovieHelper.getInstance(getApplicationContext())
                .isFavorite(getIntent().getIntExtra(EXTRA_ID, 1), getIntent().getIntExtra(EXTRA_TYPE, 1))) {
            isFavorite = true;
            ivAddToFavorite.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            isFavorite = false;
            ivAddToFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }

        if (savedInstanceState != null) {
            imageUrl = savedInstanceState.getString(IMAGE_URL);
            title = savedInstanceState.getString(TITLE);
            genres = savedInstanceState.getString(GENRES);
            releaseDate = savedInstanceState.getString(RELEASE_DATE);
            runTime = savedInstanceState.getString(RUN_TIME);
            userScore = savedInstanceState.getString(USER_SCORE);
            overview = savedInstanceState.getString(OVERVIEW);
            productionCompanies = savedInstanceState.getString(PRODUCTION_COMPANIES);

            Glide.with(DetailMovieActivity.this)
                    .load("https://image.tmdb.org/t/p/w185" + imageUrl)
                    .into(ivPoster);
            tvTitle.setText(title);
            tvGenres.setText(genres);
            tvReleaseDate.setText(releaseDate);
            tvRunTime.setText(runTime);
            tvUserScore.setText(userScore);
            tvOverview.setText(overview);
            tvProductionCompanies.setText(productionCompanies);

            contentContainer.setVisibility(View.VISIBLE);
            noContentContainer.setVisibility(View.GONE);
            pb.setVisibility(View.GONE);
        } else {
            if (getIntent().getIntExtra(EXTRA_TYPE, 1) == Movie.TYPE_MOVIE) {
                getMovieDetail();
            } else {
                getTvDetail();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(IMAGE_URL, imageUrl);
        outState.putString(TITLE, title);
        outState.putString(GENRES, genres);
        outState.putString(RELEASE_DATE, releaseDate);
        outState.putString(RUN_TIME, runTime);
        outState.putString(USER_SCORE, userScore);
        outState.putString(OVERVIEW, overview);
        outState.putString(PRODUCTION_COMPANIES, productionCompanies);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_refresh) {
            getMovieDetail();
        } else if (view.getId() == R.id.iv_add_to_favorite) {
            if (!isFavorite) {
                isFavorite = true;
                MovieHelper.getInstance(getApplicationContext()).addToFavorite(
                        new Movie(getIntent().getIntExtra(EXTRA_ID, 1), title, overview, imageUrl),
                        getIntent().getIntExtra(EXTRA_TYPE, 1)
                );
//                notifyWidgetDataSetChanged();

                ivAddToFavorite.setImageResource(R.drawable.ic_favorite_black_24dp);
            } else {
                isFavorite = false;
                MovieHelper.getInstance(getApplicationContext()).removeFromFavorite(
                        getIntent().getIntExtra(EXTRA_ID, 1),
                        getIntent().getIntExtra(EXTRA_TYPE, 1)
                );
//                notifyWidgetDataSetChanged();

                ivAddToFavorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            }
        }
    }

    private void notifyWidgetDataSetChanged() {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(getApplicationContext(), FavoriteMovieWidget.class));
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view);
    }

    private void getMovieDetail() {
        contentContainer.setVisibility(View.GONE);
        noContentContainer.setVisibility(View.GONE);
        pb.setVisibility(View.VISIBLE);

        ApiInterface apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiInterface.getMovieDetail(getIntent().getIntExtra(EXTRA_ID, -1),
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.language));

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    try {
                        JsonObject results = response.body().getAsJsonObject();
                        StringBuilder genresBuilder = new StringBuilder();
                        JsonArray genresArr = results.get("genres").getAsJsonArray();
                        for (int i = 0; i < genresArr.size(); i++) {
                            JsonObject genresObj = genresArr.get(i).getAsJsonObject();
                            genresBuilder.append(genresObj.get("name").getAsString());
                            if (i < genresArr.size() - 1) {
                                genresBuilder.append(", ");
                            }
                        }

                        StringBuilder productionCompaniesBuilder = new StringBuilder();
                        JsonArray companiesArr = results.get("production_companies").getAsJsonArray();
                        for (int i = 0; i < companiesArr.size(); i++) {
                            JsonObject companiesObj = companiesArr.get(i).getAsJsonObject();
                            productionCompaniesBuilder.append(companiesObj.get("name").getAsString());
                            if (i < companiesArr.size() - 1) {
                                productionCompaniesBuilder.append(", ");
                            }
                        }

                        imageUrl = results.get("poster_path").getAsString();
                        title = results.get("title").getAsString();
                        genres = genresBuilder.toString();
                        releaseDate = results.get("release_date").getAsString();
                        runTime = String.format("%s min", results.get("runtime").getAsString());
                        userScore = results.get("vote_average").getAsString();
                        overview = results.get("overview").getAsString();
                        productionCompanies = productionCompaniesBuilder.toString();

                        Glide.with(DetailMovieActivity.this)
                                .load("https://image.tmdb.org/t/p/w185" + imageUrl)
                                .into(ivPoster);
                        tvTitle.setText(title);
                        tvGenres.setText(genres);
                        tvReleaseDate.setText(releaseDate);
                        tvRunTime.setText(runTime);
                        tvUserScore.setText(userScore);
                        tvOverview.setText(overview);
                        tvProductionCompanies.setText(productionCompanies);

                        contentContainer.setVisibility(View.VISIBLE);
                        noContentContainer.setVisibility(View.GONE);
                        pb.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    pb.setVisibility(View.GONE);
                    contentContainer.setVisibility(View.GONE);
                    noContentContainer.setVisibility(View.VISIBLE);
                    ivErrorIcon.setImageResource(R.drawable.ic_error_outline_black_24dp);
                    tvErrorMessage.setText(R.string.response_unsuccessful);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                pb.setVisibility(View.GONE);
                contentContainer.setVisibility(View.GONE);
                noContentContainer.setVisibility(View.VISIBLE);
                ivErrorIcon.setImageResource(R.drawable.ic_signal_cellular_connected_no_internet_1_bar_black_24dp);
                tvErrorMessage.setText(R.string.response_connection_error);
            }
        });
    }

    private void getTvDetail() {
        contentContainer.setVisibility(View.GONE);
        noContentContainer.setVisibility(View.GONE);
        pb.setVisibility(View.VISIBLE);

        ApiInterface apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiInterface.getTvDetail(getIntent().getIntExtra(EXTRA_ID, -1),
                getResources().getString(R.string.api_key),
                getResources().getString(R.string.language));

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    try {
                        JsonObject results = response.body().getAsJsonObject();
                        StringBuilder genresBuilder = new StringBuilder();
                        JsonArray genresArr = results.get("genres").getAsJsonArray();
                        for (int i = 0; i < genresArr.size(); i++) {
                            JsonObject genresObj = genresArr.get(i).getAsJsonObject();
                            genresBuilder.append(genresObj.get("name").getAsString());
                            if (i < genresArr.size() - 1) {
                                genresBuilder.append(", ");
                            }
                        }

                        StringBuilder productionCompaniesBuilder = new StringBuilder();
                        JsonArray companiesArr = results.get("production_companies").getAsJsonArray();
                        for (int i = 0; i < companiesArr.size(); i++) {
                            JsonObject companiesObj = companiesArr.get(i).getAsJsonObject();
                            productionCompaniesBuilder.append(companiesObj.get("name").getAsString());
                            if (i < companiesArr.size() - 1) {
                                productionCompaniesBuilder.append(", ");
                            }
                        }

                        StringBuilder runTimesBuilder = new StringBuilder();
                        JsonArray runTimeArr = results.get("episode_run_time").getAsJsonArray();
                        for (int i = 0; i < runTimeArr.size(); i++) {
                            runTimesBuilder.append(runTimeArr.get(i).getAsString()).append(" min");
                            if (i < runTimeArr.size() - 1) {
                                runTimesBuilder.append(", ");
                            }
                        }

                        imageUrl = results.get("poster_path").getAsString();
                        title = results.get("name").getAsString();
                        genres = genresBuilder.toString();
                        releaseDate = results.get("first_air_date").getAsString();
                        runTime = runTimesBuilder.toString();
                        userScore = results.get("vote_average").getAsString();
                        overview = results.get("overview").getAsString();
                        productionCompanies = productionCompaniesBuilder.toString();

                        Glide.with(DetailMovieActivity.this)
                                .load("https://image.tmdb.org/t/p/w185" + imageUrl)
                                .into(ivPoster);
                        tvTitle.setText(title);
                        tvGenres.setText(genres);
                        tvReleaseDate.setText(releaseDate);
                        tvRunTime.setText(runTime);
                        tvUserScore.setText(userScore);
                        tvOverview.setText(overview);
                        tvProductionCompanies.setText(productionCompanies);

                        contentContainer.setVisibility(View.VISIBLE);
                        noContentContainer.setVisibility(View.GONE);
                        pb.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    pb.setVisibility(View.GONE);
                    contentContainer.setVisibility(View.GONE);
                    noContentContainer.setVisibility(View.VISIBLE);
                    ivErrorIcon.setImageResource(R.drawable.ic_error_outline_black_24dp);
                    tvErrorMessage.setText(R.string.response_unsuccessful);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                pb.setVisibility(View.GONE);
                contentContainer.setVisibility(View.GONE);
                noContentContainer.setVisibility(View.VISIBLE);
                ivErrorIcon.setImageResource(R.drawable.ic_signal_cellular_connected_no_internet_1_bar_black_24dp);
                tvErrorMessage.setText(R.string.response_connection_error);
            }
        });
    }
}
