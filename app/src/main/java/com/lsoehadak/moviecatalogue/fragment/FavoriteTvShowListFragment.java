package com.lsoehadak.moviecatalogue.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.lsoehadak.moviecatalogue.DetailMovieActivity;
import com.lsoehadak.moviecatalogue.R;
import com.lsoehadak.moviecatalogue.adapter.MovieAdapter;
import com.lsoehadak.moviecatalogue.database.MovieHelper;
import com.lsoehadak.moviecatalogue.model.Movie;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteTvShowListFragment extends Fragment implements View.OnClickListener, MovieAdapter.MovieListener {
    private final String MOVIE_LIST = "movie_list";

    private SwipeRefreshLayout contentContainer;
    private ProgressBar pb;
    private View noContentContainer;
    private ImageView ivErrorIcon;
    private TextView tvErrorMessage;

    private ArrayList<Movie> items = new ArrayList<>();
    private MovieAdapter adapter;

    public FavoriteTvShowListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView rv;
        Button btnRefresh;

        contentContainer = view.findViewById(R.id.content_container);
        rv = view.findViewById(R.id.rv);
        pb = view.findViewById(R.id.pb);
        noContentContainer = view.findViewById(R.id.no_content_container);
        ivErrorIcon = noContentContainer.findViewById(R.id.iv_error_icon);
        tvErrorMessage = noContentContainer.findViewById(R.id.tv_error_message);
        btnRefresh = noContentContainer.findViewById(R.id.btn_refresh);

        btnRefresh.setOnClickListener(this);

        contentContainer.setEnabled(false);

        adapter = new MovieAdapter(getActivity(), items, this);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (savedInstanceState != null) {
            items = savedInstanceState.getParcelableArrayList(MOVIE_LIST);
            adapter.setItems(items);

            contentContainer.setRefreshing(false);
            contentContainer.setVisibility(View.VISIBLE);
            noContentContainer.setVisibility(View.GONE);
            pb.setVisibility(View.GONE);
        } else {
            fetchData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fetchData();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(MOVIE_LIST, items);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_refresh) {
            fetchData();
        }
    }

    @Override
    public void onMovieItemClicked(Movie movie) {
        Intent intent = new Intent(getActivity(), DetailMovieActivity.class);
        intent.putExtra(DetailMovieActivity.EXTRA_TYPE, Movie.TYPE_TV);
        intent.putExtra(DetailMovieActivity.EXTRA_ID, movie.getId());
        startActivity(intent);
    }

    private void fetchData() {
        contentContainer.setVisibility(View.GONE);
        noContentContainer.setVisibility(View.GONE);
        pb.setVisibility(View.VISIBLE);
        getTvShowList();
    }

    private void getTvShowList() {
        items = MovieHelper.getInstance(getActivity().getApplicationContext()).getFavorites(Movie.TYPE_TV);

        if (items.size() > 0) {
            adapter.setItems(items);

            contentContainer.setRefreshing(false);
            contentContainer.setVisibility(View.VISIBLE);
            noContentContainer.setVisibility(View.GONE);
            pb.setVisibility(View.GONE);
        } else {
            pb.setVisibility(View.GONE);
            contentContainer.setVisibility(View.GONE);
            contentContainer.setRefreshing(false);
            noContentContainer.setVisibility(View.VISIBLE);
            ivErrorIcon.setImageResource(R.drawable.ic_local_movies_black_24dp);
            tvErrorMessage.setText(R.string.no_movies);
        }
    }

}
