package com.lsoehadak.moviecatalogue.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lsoehadak.moviecatalogue.DetailMovieActivity;
import com.lsoehadak.moviecatalogue.R;
import com.lsoehadak.moviecatalogue.adapter.MovieAdapter;
import com.lsoehadak.moviecatalogue.model.Movie;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchResultListFragment extends Fragment implements MovieAdapter.MovieListener {
    public static final String EXTRA_MOVIES = "extra_movies";

    private RecyclerView rv;
    private ConstraintLayout noContentContainer;
    private TextView tvNoContentTitle, tvNoContentMessage;

    private ArrayList<Movie> items = new ArrayList<>();
    private MovieAdapter adapter;

    public SearchResultListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rv = view.findViewById(R.id.rv);
        noContentContainer = view.findViewById(R.id.no_content_container);
        tvNoContentTitle = view.findViewById(R.id.tv_no_content_title);
        tvNoContentMessage = view.findViewById(R.id.tv_no_content_message);

        adapter = new MovieAdapter(getActivity(), items, this);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));

        fetchData();
    }

    private void fetchData() {
        if (getArguments() != null) {
            items = getArguments().getParcelableArrayList(EXTRA_MOVIES);
        }

        if (items != null) {
            if (items.size() > 0) {
                adapter.setItems(items);
                rv.setVisibility(View.VISIBLE);
                noContentContainer.setVisibility(View.GONE);
            } else {
                rv.setVisibility(View.GONE);
                noContentContainer.setVisibility(View.VISIBLE);
                if (getArguments().getInt(DetailMovieActivity.EXTRA_TYPE) == Movie.TYPE_MOVIE) {
                    tvNoContentTitle.setText(getResources().getString(R.string.no_movie_found_title));
                    tvNoContentMessage.setText(getResources().getString(R.string.no_movie_found_message));
                } else {
                    tvNoContentTitle.setText(getResources().getString(R.string.no_tv_found_title));
                    tvNoContentMessage.setText(getResources().getString(R.string.no_tv_found_message));
                }
            }
        }
    }

    @Override
    public void onMovieItemClicked(Movie movie) {
        Intent intent = new Intent(getActivity(), DetailMovieActivity.class);
        if (getArguments() != null) {
            intent.putExtra(DetailMovieActivity.EXTRA_TYPE, getArguments().getInt(DetailMovieActivity.EXTRA_TYPE));
            intent.putExtra(DetailMovieActivity.EXTRA_ID, movie.getId());
            startActivity(intent);
        }
    }
}
