package com.lsoehadak.moviecatalogue.alarmmanager;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lsoehadak.moviecatalogue.ApiInterface;
import com.lsoehadak.moviecatalogue.R;
import com.lsoehadak.moviecatalogue.RetrofitClient;
import com.lsoehadak.moviecatalogue.model.Movie;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;

public class AlarmReceiver extends BroadcastReceiver {
    public static final String TYPE_DAILY_REMINDER = "DailyReminder";
    public static final String TYPE_RELEASE_TODAY_REMINDER = "ReleaseTodayReminder";

    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_MESSAGE = "extra_message";
    public static final String EXTRA_TYPE = "extra_type";

    private final int ID_DAILY_REMINDER = 100;
    private final int ID_RELEASE_TODAY_REMINDER = 101;

    @Override
    public void onReceive(Context context, Intent intent) {
        String type = intent.getStringExtra(EXTRA_TYPE);
        String title = intent.getStringExtra(EXTRA_TITLE);
        String message = intent.getStringExtra(EXTRA_MESSAGE);
        int notificationId = type.equalsIgnoreCase(TYPE_DAILY_REMINDER) ? ID_DAILY_REMINDER : ID_RELEASE_TODAY_REMINDER;
        if (type.equals(TYPE_DAILY_REMINDER)) {
            showDailyReminderNotification(context, title, message, notificationId);
        } else {
            getReleaseTodayMovie(context, notificationId);
        }
    }

    public void setDailyReminder(Context context, String title, String message) {
        Toast.makeText(context, "daily reminder set", Toast.LENGTH_SHORT).show();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_TYPE, TYPE_DAILY_REMINDER);
        intent.putExtra(EXTRA_TITLE, title);
        intent.putExtra(EXTRA_MESSAGE, message);

        Calendar calendar = Calendar.getInstance();

        if (calendar.get(Calendar.HOUR_OF_DAY) < 7) {
            calendar.set(Calendar.HOUR_OF_DAY, 7);
        } else {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 7);
        }

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ID_DAILY_REMINDER, intent, 0);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    public void setReleaseTodayReminder(Context context, Boolean isRequestFailed) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_TYPE, TYPE_RELEASE_TODAY_REMINDER);

        Calendar calendar = Calendar.getInstance();

        if (!isRequestFailed) {
            if (calendar.get(Calendar.HOUR_OF_DAY) < 8) {
                calendar.set(Calendar.HOUR_OF_DAY, 8);
            } else {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 8);
            }
        } else {
            // request data lagi jika gagal dalam 10 menit
            calendar.add(Calendar.MINUTE, 10);
        }

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ID_RELEASE_TODAY_REMINDER, intent, 0);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    private void getReleaseTodayMovie(final Context context, final int notificationId) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


        ApiInterface apiInterface = RetrofitClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiInterface.getNewReleasedMovieList(
                context.getResources().getString(R.string.api_key),
                sdf.format(calendar.getTime()), sdf.format(calendar.getTime()));

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull retrofit2.Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    try {
                        JsonArray dataArr = response.body().get("results").getAsJsonArray();
                        if (dataArr.size() > 0) {
                            for (int i = 0; i < dataArr.size(); i++) {
                                JsonObject dataObj = dataArr.get(i).getAsJsonObject();
                                Movie movie = new Movie(
                                        dataObj.get("id").getAsInt(),
                                        dataObj.get("title").isJsonNull() ? "" : dataObj.get("title").getAsString(),
                                        dataObj.get("overview").isJsonNull() ? "" : dataObj.get("overview").getAsString(),
                                        dataObj.get("poster_path").isJsonNull() ? "" : dataObj.get("poster_path").getAsString()
                                );
                                showReleaseTodayNotification(context, movie.getTitle(),
                                        movie.getTitle() + " has been released today", i);
                            }

                            setReleaseTodayReminder(context, false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        setReleaseTodayReminder(context, false);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
                setReleaseTodayReminder(context, true);
            }
        });
    }

    private void showDailyReminderNotification(Context context, String title, String message, int notifId) {
        String CHANNEL_ID = "Channel_1";
        String CHANNEL_NAME = "Daily Reminder Notification";
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_local_movies_black_24dp)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            builder.setChannelId(CHANNEL_ID);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        Notification notification = builder.build();
        if (notificationManager != null) {
            notificationManager.notify(notifId, notification);
        }

        setDailyReminder(context, title, message);
    }

    private void showReleaseTodayNotification(Context context, String title, String message, int notifId) {
        final String GROUP_KEY_MOVIE = "group_key_movie";

        String CHANNEL_ID = "Channel_2";
        String CHANNEL_NAME = "Release Today Notification";
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_local_movies_black_24dp)
                .setContentTitle(title)
                .setContentText(message)
                .setGroup(GROUP_KEY_MOVIE)
                .setGroupSummary(true)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT);
            builder.setChannelId(CHANNEL_ID);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        Notification notification = builder.build();
        if (notificationManager != null) {
            notificationManager.notify(notifId, notification);
        }
    }

    public void cancelAlarm(Context context, String type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        int requestCode = type.equalsIgnoreCase(TYPE_DAILY_REMINDER) ? ID_DAILY_REMINDER : ID_RELEASE_TODAY_REMINDER;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        pendingIntent.cancel();

        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }
}
