package com.lsoehadak.moviecatalogue.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.lsoehadak.moviecatalogue.model.Movie;

import java.util.ArrayList;

public class MovieHelper {
    private static DatabaseHelper databaseHelper;
    private static MovieHelper instance;

    private static SQLiteDatabase database;

    private MovieHelper(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public static MovieHelper getInstance(Context context) {
        if (instance == null) {
            instance = new MovieHelper(context);
        }

        return instance;
    }

    public void open() {
        database = databaseHelper.getWritableDatabase();
    }

    public void close() {
        databaseHelper.close();

        if (database.isOpen()) {
            database.close();
        }
    }

    public void addToFavorite(Movie movie, int type) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.MovieColumns.TYPE, type);
        contentValues.put(DatabaseContract.MovieColumns._ID, movie.getId());
        contentValues.put(DatabaseContract.MovieColumns.TITLE, movie.getTitle());
        contentValues.put(DatabaseContract.MovieColumns.OVERVIEW, movie.getOverview());
        contentValues.put(DatabaseContract.MovieColumns.POSTER_URL, movie.getPosterUrl());

        database.insert(DatabaseContract.MovieColumns.TABLE_NAME, null, contentValues);
    }

    public void removeFromFavorite(int id, int type) {
        database.delete(DatabaseContract.MovieColumns.TABLE_NAME,
                DatabaseContract.MovieColumns._ID + " = ? AND " + DatabaseContract.MovieColumns.TYPE + " = ?",
                new String[]{String.valueOf(id), String.valueOf(type)});
    }

    public ArrayList<Movie> getFavorites(int type) {
        ArrayList<Movie> items = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + DatabaseContract.MovieColumns.TABLE_NAME;
        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                if (cursor.getInt(1) == type) {
                    Movie item = new Movie(
                            cursor.getInt(0),
                            cursor.getString(2),
                            cursor.getString(3),
                            cursor.getString(4));

                    items.add(item);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        return items;
    }

    Cursor getFavoritesCursor() {
        String selectQuery = "SELECT * FROM " + DatabaseContract.MovieColumns.TABLE_NAME;
        Cursor cursor = database.rawQuery(selectQuery, null);
        return cursor;
    }

    public boolean isFavorite(int id, int type) {
        String selectQuery = "SELECT * FROM " + DatabaseContract.MovieColumns.TABLE_NAME + " WHERE " + DatabaseContract.MovieColumns._ID + " ='" + id + "' AND "
                + DatabaseContract.MovieColumns.TYPE + "= '" + type + "'";
        Cursor cursor = database.rawQuery(selectQuery, null);

        boolean isFavorite;
        isFavorite = cursor.getCount() > 0;
        cursor.close();

        return isFavorite;
    }
}
