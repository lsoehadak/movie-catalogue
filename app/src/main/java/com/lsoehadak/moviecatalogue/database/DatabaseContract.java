package com.lsoehadak.moviecatalogue.database;

import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseContract {
    public static final String AUTHORITY = "com.lsoehadak.moviecatalogue";
    private static final String SCHEME = "content";

    public static class MovieColumns implements BaseColumns {
        // Type adalah flag yang menunjukkan data ini merupakah Movie / TV Show
        static String TABLE_NAME = "movie";
        static String TYPE = "type";
        static String TITLE = "title";
        static String OVERVIEW = "overview";
        static String POSTER_URL = "poster_url";

        public static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME)
                .build();
    }
}
